#ifndef MAIN_WIDGET_H
#define MAIN_WIDGET_H

#include <QWidget>
#include <QString>

namespace Ui
{
class main_widget;
}

class main_widget : public QWidget
{
    Q_OBJECT

public:
    explicit main_widget(QWidget *parent = 0);
    ~main_widget();

private:
    void make_connections_();

private:
    void generate_(const QString& class_name);
private:
    QString m_h_file;
    QString m_cpp_file;

private:
    Ui::main_widget *ui;

private slots:
    void generate_clicked();
};

#endif // MAIN_WIDGET_H
