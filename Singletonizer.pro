#-------------------------------------------------
#
# Project created by QtCreator 2014-03-30T15:31:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Singletonizer
TEMPLATE = app


SOURCES += main.cpp\
        main_widget.cpp

HEADERS  += main_widget.h

FORMS    += main_widget.ui
