#include "main_widget.h"
#include "ui_main_widget.h"

main_widget::main_widget(QWidget *parent)
    : QWidget(parent),
      ui(new Ui::main_widget)
{
    ui->setupUi(this);

    this->setLayout(ui->main_layout);
    make_connections_();
}

main_widget::~main_widget()
{
    delete ui;
}

void main_widget::make_connections_()
{
    connect(ui->generate_btn, SIGNAL(clicked()),
            this, SLOT(generate_clicked()));
}

void main_widget::generate_(const QString& class_name)
{
    m_h_file = "#ifndef " + class_name.toUpper() + "_H\n" \
            "#define " + class_name.toUpper() + "_H\n\n" \
            "class " + class_name + "\n" \
            "{\n" \
            "private:\n" \
            "\t" + class_name + "();\n" \
            "\t" + class_name + "(const " + class_name + "&);\n" \
            "\t" + class_name + "& operator=(const " + class_name + "&);\n" \
            "\t~" + class_name + "();\n\n" \
            "public:\n" \
            "\tstatic " + class_name + "* s_instance;\n" \
            "\tstatic " + class_name + "* get_instance();\n" \
            "\tstatic void remove_instance();\n" \
            "};\n\n" \
            "#endif " + "// " + class_name.toUpper();

    m_cpp_file = "#include \"" + class_name + "\"\n\n" \
            + class_name + "* " + class_name + "::s_instance = 0;\n" \
            + class_name + "* " + class_name + "::get_instance()\n" \
            "{\n" \
            "\tif(!s_instance)\n" \
            "\t{\n" \
            "\t\t" + class_name + "* tmp = new " + class_name + "();\n" \
            "\t\ts_instance = tmp;\n" \
            "\t}\n" \
            "\treturn s_instance;\n" \
            "}\n\n" \
            "void " + class_name + "::remove_instance()\n" \
            "{\n" \
            "\tdelete s_instance;\n" \
            "\ts_instance = 0;\n" \
            "}";
}

void main_widget::generate_clicked()
{
    if(!ui->class_name_line_edit->text().isEmpty())
    {
        generate_(ui->class_name_line_edit->text());
        ui->h_file_text_edit->setText(m_h_file);
        ui->cpp_file_text_edit->setText(m_cpp_file);
    }
}
